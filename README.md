
# Introduction
Require install the latest version of [docker-machine](https://docs.docker.com/docker-for-mac/install/) and [docker-compose](https://docs.docker.com/compose/install/)

# Table of Contents
1. [Files & Directories structure](#files--directories-structure)
    - [Project structure](#project-structure)
    - [API package structure](#api-package-structure)
2. [Usage](#usage)
    - [Start the project with Docker](#start-the-project-with-docker)
    - [Start the project using bash script files](#start-the-project-using-bash-script-files)
3. [API endpoints](#api-endpoints)
	  - [1. Create a friend connection](#create-a-friend-connection)
	  - [2. Retrieve the friends list](#retrieve-the-friends-list)
	  - [3. Retrieve the common friends list](#retrieve-the-common-friends-list)
	  - [4. Subscribe to updates from an email address](#subscribe-to-updates-from-an-email-address)
	  - [5. Block updates from an email address](#block-updates-from-an-email-address)
	  - [6. Retrieve all email addresses that can receive updates from an email address](#retrieve-all-email-addresses-that-can-receive-updates-from-an-email-address)
    - [Error response](#error-response)
4. [Datasheet](#datasheet)
---
## Files & Directories structure

Describe for all files and directories structure using in this project.

### Project structure
```
.
├── server                 # Backend codebase
│   ├── api
│   ├── internal           # Internal package, only share to internal project
│   ├── migrations         # Contain SQL scripts
│   ├── migrations_test    # Contain SQL scripts include mock-data for testing
│   ├── model
│   ├── routes
│   ├── scripts
│   │   ├── set-up.sh      # Automation script using for docker
│   ├── Dockerfile
│   ├── go.mod
│   ├── go.sum
│   ├── main.go
├── db
│   ├── database.db        # Main database - generate when run run.sh or docker
│   ├── database_test.db   # Test database - generate when run test.sh
├── .env
├── docker-compose.yml
├── README.md
└── ...
```

###  API package structure

- One `api` package can have many handlers.
- One `handler` contains controller - model - repository.
	- Controller will handle request, response and validate.
	- Model contains all structs/interface and mapping.
	- Repository handles the request to get data from database.

```
api
├── controller_base.go     # The controller inside handler should inherit this
├── repository_base.go     # The repository inside handler should inherit this
├── handler
│   ├── controller.go
│   ├── model.go
│   ├── repository.go
└── ...
```

## Usage

Clone this project and put follow this path `$GOPATH/src/projects/challenge`, all codebase should place inside this folder


### Start the project with Docker

**Required: install Docker and docker-compose**, follow the command below to start the project with docker.

01 - Run the command to build the docker containers
```bash
docker-compose build
```

02 - Start all docker containers
```bash
docker-compose up
```
or
```bash
docker-compose up -d
```

After all docker containers are up, please check on this URL [http://localhost:1313/](http://localhost:1313/)

**TO SHUTDOWN DOCKER**
```bash
docker-compose down
```

### Start the project using bash script files
**Required: installed Golang and SQLite3**, follow the command below to start the project with bash script.

01 - Run the command to install and config some stuffs
```bash
./install.sh
```

02 - Start server
```bash
./run.sh
```

**TO RUN ALL THE TESTS**
```bash
./run.sh
```

##  API endpoints

### 1. Create a friend connection
```
POST /friend-management
```
#### Request
| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:-------------|
| friends    | [2]string | yes      | An array of string contains maximum is two emails address |

```json
{
	"friends": [
		"andy@example.com",
		"john@example.com"
	]
}
```

#### Response
| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:-------------|
| success    | bool      | yes      |  |

```json
{
	"success": true
}
```

---
### 2. Retrieve the friends list

```
GET /friend-management
```

#### Request

| Attribute | Type   | Required | Description |
| :-------- |:------:|:--------:|:-------------|
| email     | string | yes      | A string is an email address |

```json
{
	"email": "andy@example.com"
}
```

#### Response

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:------------|
| success    | bool      | yes      |             |
| friends    | []string  | yes      |             |
| count      | int       | yes      |             |

```json
{
  "success": true,
  "friends" : [
    "john@example.com"
  ],
  "count" : 1
}
```

---
### 3. Retrieve the common friends list

```
GET /common-friends
```

#### Request

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:-------------|
| friends    | [2]string | yes      | An array of string contains maximum is two emails address |

```json
{
	"friends": [
		"andy@example.com",
		"john@example.com"
	]
}
```

#### Response

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:------------|
| success    | bool      | yes      |             |
| friends    | []string  | yes      |             |
| count      | int       | yes      |             |

```json
{
	"success": true,
	"friends": [
		"common@example.com"
	],
	count: 1
}
```

---
### 4. Subscribe to updates from an email address

```
POST /subscribe
```

#### Request

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:-------------|
| requestor  | string    | yes      | A string is an email address |
| target     | string    | yes      | A string is an email address |

```json
{
  "requestor": "lisa@example.com",
  "target": "john@example.com"
}
```

#### Response

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:------------|
| success    | bool      | yes      |             |

```json
{
	"success": true
}
```

---
### 5. Block updates from an email address

```
POST /block
```

#### Request

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:-------------|
| requestor  | string    | yes      | A string is an email address |
| target     | string    | yes      | A string is an email address |

```json
{
  "requestor": "lisa@example.com",
  "target": "john@example.com"
}
```

#### Response

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:------------|
| success    | bool      | yes      |             |

```json
{
	"success": true
}
```

---
### 6. Retrieve all email addresses that can receive updates from an email address

```
GET /receive-update
```

#### Request

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:-------------|
| sender     | string    | yes      | A string is an email address |
| text       | string    | yes      | A string is a message content |

```json
{
  "sender": "john@example.com",
  "text": "Hello World! kate@example.com"
}
```

#### Response

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:------------|
| success    | bool      | yes      |             |
| recipients | []string  | yes      |             |


```json
{
  "success": true,
  "recipients": [
    "lisa@example.com",
    "kate@example.com"
  ]
}
```

---
### Error response

Default struct for all error response

#### Response

| Attribute  | Type      | Required | Description |
| :--------- |:---------:|:--------:|:------------|
| success    | bool      | yes      |             |
| error      | string    | yes      |             |


```json
{
  "success": true,
  "error": "an error has occurred. please try again later"
}
```

## Datasheet

Below is the list of email available to use

| Email              | Email                 |
| ------------------ | --------------------- |
| andy@example.com   | felis@example.com     |
| john@example.com   | ali@example.com       |
| common@example.com | non@example.com       |
| lisa@example.com   | pharetra@example.com  |
| kate@example.com   | massa@example.com     |
| magna@example.com  | odio@example.com      |
| sit@example.com    | ante@example.com      |
| pede@example.com   | edu@example.com       |
| vel@example.com    | dui@example.com       |
| corper@example.com | consequat@example.com |











