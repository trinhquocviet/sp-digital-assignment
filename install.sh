#!/bin/sh
## Script to setup your application
## e.g. 
## npm install
## mvn install
## bundle install
## go build
# Download goose to run migrations
go get -u github.com/pressly/goose/cmd/goose

# install sqlite3
apt-get -y update
apt-get install -y sqlite3 libsqlite3-dev

# Update env
cp -n .env.example .env

cd server

# install golang packages
go get -u

# move out
cd ../