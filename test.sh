#!/bin/sh
## Script to test your application
## e.g. 
## npm test
## mvn clean test
## python test_unit.py
## bundle exec rails rpsec spec
## go test -v

# ./install.sh

# Change directory to server
cd server

# export enviroment
export $(grep -v '^#' ../.env | xargs) && \
export API_DATABASE=$API_DATABASE_TEST

# remove test database
if test -f $API_DATABASE_TEST; then
  rm -rf $API_DATABASE_TEST
fi

sleep 1

while test -f "$API_DATABASE_TEST"; do
  sleep 1
done

# run migration && start test
$GOPATH/bin/goose -dir migrations_test sqlite3 "${API_DATABASE_TEST}" up && \
go test ./... -count=1

# Go out
cd ../