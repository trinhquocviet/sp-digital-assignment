package block

import (
	"fmt"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	msg "projects/challenge/server/internal/message"
	"strings"
	"testing"
)

var (
	defaultSuccesResJSONForBlockUser = `{"success":true}`
)

func TestController_Block(t *testing.T) {
	requestFormat := `{"requestor": "%s","target":"%s"}`
	errorResFormat := `{"success":false,"error":"%s"}`

	tests := []struct {
		name           string
		reqJSON        string
		wantHTTPStatus int
		wantResJSON    string
	}{
		{
			name:           "Andy block John, John is a friend with Andy",
			reqJSON:        fmt.Sprintf(requestFormat, "andy@example.com", "john@example.com"),
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    defaultSuccesResJSONForBlockUser,
		},
		{
			name:           "Andy block non exist user",
			reqJSON:        fmt.Sprintf(requestFormat, "andy@example.com", "nonexistuser@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.OneOfUsersDoNotExist),
		},
		{
			name:           "Andy block himself",
			reqJSON:        fmt.Sprintf(requestFormat, "andy@example.com", "andy@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.OneOfUsersDoNotExist),
		},
		{
			name:           "Non exist user block Andy",
			reqJSON:        fmt.Sprintf(requestFormat, "nonexistuser@example.com", "andy@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.OneOfUsersDoNotExist),
		},
		{
			name:           "Andy block Pede, Andy isn't friend with Pede",
			reqJSON:        fmt.Sprintf(requestFormat, "andy@example.com", "pede@example.com"),
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    defaultSuccesResJSONForBlockUser,
		},
		{
			name:           "blank request",
			reqJSON:        `{}`,
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.RequestParamsMismatch),
		},
		{
			name:           "blank requestor block pede",
			reqJSON:        fmt.Sprintf(requestFormat, "", "pede@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.RequestParamsMismatch),
		},
		{
			name:           "Andy block blank target",
			reqJSON:        fmt.Sprintf(requestFormat, "andy@example.com", ""),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.RequestParamsMismatch),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var ctr Controller
			e := echo.New()
			req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(tt.reqJSON))
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			rec := httptest.NewRecorder()
			c := e.NewContext(req, rec)

			if assert.NoError(t, ctr.Block(c)) {
				assert.Equal(t, tt.wantHTTPStatus, rec.Code)
				assert.Equal(t, tt.wantResJSON, rec.Body.String())
			}
		})
	}
}
