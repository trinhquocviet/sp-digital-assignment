package block

import (
	"fmt"
	logrus "github.com/Sirupsen/logrus"
	api "projects/challenge/server/api"
	msg "projects/challenge/server/internal/message"
	model "projects/challenge/server/model"
)

// Repository use to interact with database
type Repository struct {
	api.RepositoryBase
}

// Block do block and unsubscribe for an user
// ! If userA block userB, remove the subscription from userA to userB
func (r *Repository) Block(req BlockReq) (err error) {
	r.OpenConnection()
	defer r.DB.Close()

	// get user info
	userInfos, err := r.QueryUserInfoFromEmailList([]string{req.Requestor, req.Target})
	if err != nil || len(userInfos) != 2 {
		logrus.Error("Subscribe error: ", err)
		err = fmt.Errorf(msg.OneOfUsersDoNotExist)
		return
	}

	// make subscriptions
	tx := r.DB.Begin()

	err = tx.Exec(
		`INSERT OR IGNORE INTO blocklist (user_id, friend_id) VALUES (?,?)`,
		userInfos[0].ID, userInfos[1].ID,
	).Error

	if err != nil {
		tx.Rollback()
		logrus.Error("Block Error: ", err)
		err = fmt.Errorf(msg.CanNotBlockAUser)
		return
	}

	// remove subscription
	err = tx.Table(
		"subscriptions",
	).Where(
		"(user_id = ? AND friend_id = ?)", userInfos[0].ID, userInfos[1].ID,
	).Delete(&model.Subscription{}).Error

	if err != nil {
		tx.Rollback()
		logrus.Error("Block Error: ", err)
		err = fmt.Errorf(msg.CanNotBlockAUser)
		return
	}

	tx.Commit()
	return
}
