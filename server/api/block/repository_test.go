package block

import (
	"testing"
)

func TestRepository_Block(t *testing.T) {

	tests := []struct {
		name    string
		req     BlockReq
		wantErr bool
	}{
		{
			name:    "Corper block Common",
			req:     BlockReq{Requestor: "corper@example.com", Target: "vel@example.com"},
			wantErr: false,
		},
		{
			name:    "Sit block Andy",
			req:     BlockReq{Requestor: "sit@example.com", Target: "andy@example.com"},
			wantErr: false,
		},
		{
			name:    "non exist user block Andy",
			req:     BlockReq{Requestor: "nonexistuser@example.com", Target: "andy@example.com"},
			wantErr: true,
		},
		{
			name:    "non exist user block non exist user",
			req:     BlockReq{Requestor: "nonexistuser1@example.com", Target: "nonexistuser2@example.com"},
			wantErr: true,
		},
		{
			name:    "Sit block non exist user",
			req:     BlockReq{Requestor: "sit@example.com", Target: "nonexistuser@example.com"},
			wantErr: true,
		},
		{
			name:    "Blank req",
			req:     BlockReq{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var r Repository
			if err := r.Block(tt.req); (err != nil) != tt.wantErr {
				t.Errorf("Repository.Block() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
