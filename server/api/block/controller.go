package block

import (
	"github.com/labstack/echo"
	api "projects/challenge/server/api"
	internal "projects/challenge/server/internal"
)

// Controller the handler function extend from api/controller_base.go
type Controller struct {
	api.ControllerBase
	repository Repository
	validator  internal.Validator
}

// Block will let one user block another user
// ! If they are connected as friends, then userA will no longer receive notifications from userB
// ! If they are not connected as friends, then no new friends connection can be added
func (ctr *Controller) Block(c echo.Context) error {
	// Build query
	var req BlockReq
	if err := c.Bind(&req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Validate
	if err := ctr.validator.Validate(req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Execute
	err := ctr.repository.Block(req)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	return ctr.SuccessRes(c)
}
