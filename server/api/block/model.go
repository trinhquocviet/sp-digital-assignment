package block

// BlockReq the Struct for binding data from request
type BlockReq struct {
	Requestor string `json:"requestor" validate:"required,email"`
	Target    string `json:"target" validate:"required,email"`
}
