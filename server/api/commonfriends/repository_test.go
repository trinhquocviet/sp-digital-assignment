package commonfriends

import (
	model "projects/challenge/server/model"
	"reflect"
	"testing"
)

func TestRepository_GetCommonFriends(t *testing.T) {
	tests := []struct {
		name         string
		req          []string
		wantUserList []model.User
		wantErr      bool
	}{
		{
			name:         "Get common friend of Andy and John",
			req:          []string{"andy@example.com", "john@example.com"},
			wantUserList: []model.User{model.User{Email: "common@example.com"}},
			wantErr:      false,
		},
		{
			name:         "Get common friend of non exist user and non exist user",
			req:          []string{"nonexistuser1@example.com", "nonexistuser1@example.com"},
			wantUserList: []model.User{},
			wantErr:      true,
		},
		{
			name:         "Get common friend of Lisa and Vel",
			req:          []string{"lisa@example.com", "vel@example.com"},
			wantUserList: []model.User{},
			wantErr:      false,
		},
		{
			name:         "Get common friend of Andy and himself",
			req:          []string{"andy@example.com", "andy@example.com"},
			wantUserList: []model.User{},
			wantErr:      true,
		},
		{
			name:         "Empty Req",
			req:          []string{},
			wantUserList: []model.User{},
			wantErr:      true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var r Repository
			rawUserList, err := r.GetCommonFriends(tt.req)
			gotUserList := []model.User{}
			if len(rawUserList) > 0 {
				for _, u := range rawUserList {
					gotUserList = append(gotUserList, model.User{Email: u.Email})
				}
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("Repository.GetCommonFriends() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(gotUserList) != len(tt.wantUserList) || !reflect.DeepEqual(gotUserList, tt.wantUserList) {
				t.Errorf("Repository.GetCommonFriends() = %v, want %v", gotUserList, tt.wantUserList)
			}
		})
	}
}
