package commonfriends

import (
	"fmt"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	msg "projects/challenge/server/internal/message"
	"strings"
	"testing"
)

func TestController_GetCommonFriends(t *testing.T) {
	requestFormat := `{"friends":["%s","%s"]}`
	errorResFormat := `{"success":false,"error":"%s"}`

	tests := []struct {
		name           string
		customReqJSON  string
		wantHTTPStatus int
		wantResJSON    string
	}{
		{
			name:           "Get common friend of Andy and John",
			customReqJSON:  fmt.Sprintf(requestFormat, "andy@example.com", "john@example.com"),
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    `{"success":true,"friends":["common@example.com"],"count":1}`,
		},
		{
			name:           "Get common friend of Andy and non exist user",
			customReqJSON:  fmt.Sprintf(requestFormat, "andy@example.com", "nonexistuser@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.OneOfUsersDoNotExist),
		},
		{
			name:           "Get common friend of 2 non exist user",
			customReqJSON:  fmt.Sprintf(requestFormat, "nonexistuser@example.com", "nonexistuser2@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.OneOfUsersDoNotExist),
		},
		{
			name:           "Get common friend of Andy and Vel",
			customReqJSON:  fmt.Sprintf(requestFormat, "andy@example.com", "vel@example.com"),
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    `{"success":true,"friends":[],"count":0}`,
		},
		{
			name:           "Empty request",
			customReqJSON:  `{}`,
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.RequestParamsMismatch),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var ctr Controller
			e := echo.New()
			req := httptest.NewRequest(http.MethodGet, "/", strings.NewReader(tt.customReqJSON))
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			rec := httptest.NewRecorder()
			c := e.NewContext(req, rec)

			if assert.NoError(t, ctr.GetCommonFriends(c)) {
				assert.Equal(t, tt.wantHTTPStatus, rec.Code)
				assert.Equal(t, tt.wantResJSON, rec.Body.String())
			}
		})
	}
}
