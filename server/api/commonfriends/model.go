package commonfriends

import (
	api "projects/challenge/server/api"
	model "projects/challenge/server/model"
)

// GetCommonFriendsReq the Struct for binding data from request
type GetCommonFriendsReq struct {
	Friends []string `json:"friends" validate:"required,dive,email"`
}

// GetCommonFriendsRes the struct is response
type GetCommonFriendsRes struct {
	api.DefaultSuccessRes
	Friends []string `json:"friends" validate:"required,dive,email"`
	Count   int      `json:"count" validate:"required,numeric"`
}

// Mapping is the function to map from model to response model
func (g *GetCommonFriendsRes) Mapping(userList []model.User) {
	g.Success = true
	if len(userList) > 0 {
		for _, user := range userList {
			g.Friends = append(g.Friends, user.Email)
		}
	} else {
		g.Friends = []string{}
	}

	g.Count = len(userList)
}
