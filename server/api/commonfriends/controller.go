package commonfriends

import (
	"github.com/labstack/echo"
	api "projects/challenge/server/api"
	internal "projects/challenge/server/internal"
)

// Controller the handler function extend from api/controller-base.go
type Controller struct {
	api.ControllerBase
	repository Repository
	validator  internal.Validator
}

// GetCommonFriends will fetch and get all users is common friend
func (ctr *Controller) GetCommonFriends(c echo.Context) error {
	// Build query
	var req GetCommonFriendsReq
	if err := c.Bind(&req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Validate
	if err := ctr.validator.Validate(req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Execute
	userList, err := ctr.repository.GetCommonFriends(req.Friends)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Mapping
	var res GetCommonFriendsRes
	res.Mapping(userList)

	return ctr.SuccessRes(c, res)
}
