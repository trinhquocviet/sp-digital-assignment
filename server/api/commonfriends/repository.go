package commonfriends

import (
	"fmt"
	logrus "github.com/Sirupsen/logrus"
	api "projects/challenge/server/api"
	msg "projects/challenge/server/internal/message"
	model "projects/challenge/server/model"
)

// Repository use to interact with database
type Repository struct {
	api.RepositoryBase
}

// GetCommonFriends query to get all friend of email
func (r *Repository) GetCommonFriends(friends []string) (userList []model.User, err error) {
	r.OpenConnection()
	defer r.DB.Close()
	userList = []model.User{}

	// get user info
	var userInfos []*model.User
	err = r.DB.Table(
		"users",
	).Select(
		"id",
	).Where(
		"email IN (?)", friends,
	).Find(&userInfos).Error

	if err != nil || len(userInfos) != 2 {
		logrus.Error("GetCommonFriends Error: ", err)
		err = fmt.Errorf(msg.OneOfUsersDoNotExist)
		return
	}

	// get list common friends
	queryExprFriendshipA := r.DB.Table("friendships").Select(
		"CASE WHEN user_id = ? THEN friend_id ELSE user_id END AS friend_id", userInfos[0].ID,
	).Where(
		"user_id = ? OR friend_id = ?", userInfos[0].ID, userInfos[0].ID,
	).QueryExpr()

	queryExprFriendshipB := r.DB.Table("friendships").Select(
		"CASE WHEN user_id = ? THEN friend_id ELSE user_id END AS friend_id", userInfos[1].ID,
	).Where(
		"user_id = ? OR friend_id = ?", userInfos[1].ID, userInfos[1].ID,
	).QueryExpr()

	// get query common friends
	queryExprCommonFriends := r.DB.Raw(
		`SELECT f_a.friend_id
		FROM (?) AS f_a
		INNER JOIN (?) AS f_b
		ON f_a.friend_id = f_b.friend_id
		`, queryExprFriendshipA, queryExprFriendshipB,
	).QueryExpr()

	// get common friend detail
	err = r.DB.Table(
		"users",
	).Select(
		"email",
	).Where(
		"id IN (?)", queryExprCommonFriends,
	).Find(&userList).Error

	if err != nil {
		logrus.Error("GetCommonFriends Error: ", err)
		userList = []model.User{}
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	return
}
