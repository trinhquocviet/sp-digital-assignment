package receiveupdate

import (
	api "projects/challenge/server/api"
	model "projects/challenge/server/model"
)

// ReceiveUpdateReq the Struct for binding data from request
type ReceiveUpdateReq struct {
	Sender string `json:"sender" validate:"required,email"`
	Text   string `json:"text" validate:"required"`
}

// ReceiveUpdateRes the struct is response
type ReceiveUpdateRes struct {
	api.DefaultSuccessRes
	Recipients []string `json:"recipients" validate:"required,dive,email"`
}

// Mapping is the function to map from model to response model
func (g *ReceiveUpdateRes) Mapping(userList []model.User) {
	g.Success = true
	if len(userList) > 0 {
		for _, user := range userList {
			g.Recipients = append(g.Recipients, user.Email)
		}
	} else {
		g.Recipients = []string{}
	}
}
