package receiveupdate

import (
	"fmt"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	msg "projects/challenge/server/internal/message"
	"reflect"
	"strings"
	"testing"
)

func TestController_getListEmailMentioned(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  []string
	}{
		{
			name:  "mention Kate",
			input: "Hello World! kate@example.com",
			want:  []string{"kate@example.com"},
		},
		{
			name:  "mention Kate and John",
			input: "Hello kate@example.com and john@example.com",
			want:  []string{"kate@example.com", "john@example.com"},
		},
		{
			name:  "no mention",
			input: "This text without mention",
			want:  []string{},
		},
		{
			name:  "mention Kate but typo with email address",
			input: "Hello kate@exa",
			want:  []string{},
		},
		{
			name:  "blank input",
			input: "",
			want:  []string{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var ctr Controller
			if got := ctr.getListEmailMentioned(tt.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Controller.getListEmailMentioned() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestController_ReceiveUpdate(t *testing.T) {
	requestFormat := `{"sender":"%s","text":"%s"}`
	successResFormat := `{"success":true,"recipients":[%s]}`
	errorResFormat := `{"success":false,"error":"%s"}`

	tests := []struct {
		name           string
		customReqJSON  string
		wantHTTPStatus int
		wantResJSON    string
	}{
		{
			name:           "Get friends will receive an update from Common",
			customReqJSON:  fmt.Sprintf(requestFormat, "common@example.com", "Hello everybody!"),
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    fmt.Sprintf(successResFormat, `"andy@example.com","john@example.com"`),
		},
		{
			name:           "Magna mentioned Sit but Sit is blocking Magna",
			customReqJSON:  fmt.Sprintf(requestFormat, "magna@example.com", "Hello sit@example.com!"),
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    fmt.Sprintf(successResFormat, `"pede@example.com"`),
		},
		{
			name:           "Andy mentioned Pede",
			customReqJSON:  fmt.Sprintf(requestFormat, "andy@example.com", "Hello pede@example.com!"),
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    fmt.Sprintf(successResFormat, `"common@example.com","pede@example.com"`),
		},
		{
			name:           "Andy mentioned Sit and Pede",
			customReqJSON:  fmt.Sprintf(requestFormat, "andy@example.com", "Hello sit@example.com and pede@example.com!"),
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    fmt.Sprintf(successResFormat, `"common@example.com","pede@example.com"`),
		},
		{
			name:           "Andy send blank message",
			customReqJSON:  fmt.Sprintf(requestFormat, "andy@example.com", ""),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.RequestParamsMismatch),
		},
		{
			name:           "blank Json",
			customReqJSON:  "{}",
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.RequestParamsMismatch),
		},
		{
			name:           "Sender isn't email address",
			customReqJSON:  fmt.Sprintf(requestFormat, "andy", "Hello sit@example.com and pede@example.com!"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.RequestParamsMismatch),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var ctr Controller
			e := echo.New()
			req := httptest.NewRequest(http.MethodGet, "/", strings.NewReader(tt.customReqJSON))
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			rec := httptest.NewRecorder()
			c := e.NewContext(req, rec)

			if assert.NoError(t, ctr.ReceiveUpdate(c)) {
				assert.Equal(t, tt.wantHTTPStatus, rec.Code)
				assert.Equal(t, tt.wantResJSON, rec.Body.String())
			}
		})
	}
}
