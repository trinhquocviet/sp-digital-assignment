package receiveupdate

import (
	model "projects/challenge/server/model"
	"reflect"
	"testing"
)

func TestRepository_getUserIDFromBlocklist(t *testing.T) {
	tests := []struct {
		name        string
		blocklist   []model.Blocklist
		wantUserIDs []uint
	}{
		{
			name:        "Empty blocklist",
			blocklist:   []model.Blocklist{},
			wantUserIDs: []uint{},
		},
		{
			name:        "1 blocklist",
			blocklist:   []model.Blocklist{model.Blocklist{UserID: 1}},
			wantUserIDs: []uint{1},
		},
		{
			name:        "2 blocklist",
			blocklist:   []model.Blocklist{model.Blocklist{UserID: 1}, model.Blocklist{UserID: 2}},
			wantUserIDs: []uint{1, 2},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var r Repository
			if gotUserIDs := r.getUserIDFromBlocklist(tt.blocklist); !reflect.DeepEqual(gotUserIDs, tt.wantUserIDs) {
				t.Errorf("Repository.getUserIDFromBlocklist() = %v, want %v", gotUserIDs, tt.wantUserIDs)
			}
		})
	}
}

func TestRepository_getUserIDFromSubscriptions(t *testing.T) {
	tests := []struct {
		name          string
		subscriptions []model.Subscription
		wantUserIDs   []uint
	}{
		{
			name:          "Empty subscription",
			subscriptions: []model.Subscription{},
			wantUserIDs:   []uint{},
		},
		{
			name:          "1 subscription",
			subscriptions: []model.Subscription{model.Subscription{UserID: 1}},
			wantUserIDs:   []uint{1},
		},
		{
			name:          "2 subscriptions",
			subscriptions: []model.Subscription{model.Subscription{UserID: 1}, model.Subscription{UserID: 2}},
			wantUserIDs:   []uint{1, 2},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var r Repository
			if gotUserIDs := r.getUserIDFromSubscriptions(tt.subscriptions); !reflect.DeepEqual(gotUserIDs, tt.wantUserIDs) {
				t.Errorf("Repository.getUserIDFromSubscriptions() = %v, want %v", gotUserIDs, tt.wantUserIDs)
			}
		})
	}
}

func TestRepository_ReceiveUpdate(t *testing.T) {
	tests := []struct {
		name         string
		sender       string
		mentioned    []string
		wantUserList []model.User
		wantErr      bool
	}{
		{
			name:         "Get receive update from Common and no mentioned",
			sender:       "common@example.com",
			mentioned:    []string{},
			wantUserList: []model.User{model.User{Email: "andy@example.com"}, model.User{Email: "john@example.com"}},
			wantErr:      false,
		},
		{
			name:         "Get receive update from Phone and no mentioned",
			sender:       "phone@example.com",
			mentioned:    []string{},
			wantUserList: []model.User{},
			wantErr:      false,
		},
		{
			name:         "Get receive update from Phone and mentioned Kate",
			sender:       "phone@example.com",
			mentioned:    []string{"kate@example.com"},
			wantUserList: []model.User{model.User{Email: "kate@example.com"}},
			wantErr:      false,
		},
		{
			name:         "Get receive update from Phone and mentioned himself",
			sender:       "phone@example.com",
			mentioned:    []string{"phone@example.com"},
			wantUserList: []model.User{},
			wantErr:      false,
		},
		{
			name:         "Get receive update from no sender",
			sender:       "",
			mentioned:    []string{},
			wantUserList: []model.User{},
			wantErr:      true,
		},
		{
			name:         "Get receive update from non exist user",
			sender:       "nonexistuser@example.com",
			mentioned:    []string{},
			wantUserList: []model.User{},
			wantErr:      true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var r Repository
			rawUserList, err := r.ReceiveUpdate(tt.sender, tt.mentioned)
			gotUserList := []model.User{}
			if len(rawUserList) > 0 {
				for _, u := range rawUserList {
					gotUserList = append(gotUserList, model.User{Email: u.Email})
				}
			}

			if (err != nil) != tt.wantErr {
				t.Errorf("Repository.ReceiveUpdate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotUserList, tt.wantUserList) {
				t.Errorf("Repository.ReceiveUpdate() = %v, want %v", gotUserList, tt.wantUserList)
			}
		})
	}
}
