package receiveupdate

import (
	"github.com/labstack/echo"
	api "projects/challenge/server/api"
	internal "projects/challenge/server/internal"
	"regexp"
)

// Controller the handler function extend from api/controller_base.go
type Controller struct {
	api.ControllerBase
	repository Repository
	validator  internal.Validator
}

// ReceiveUpdate will get list
func (ctr *Controller) ReceiveUpdate(c echo.Context) error {
	// Build query
	var req ReceiveUpdateReq
	if err := c.Bind(&req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Validate
	if err := ctr.validator.Validate(req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Execute
	var mentionedList = ctr.getListEmailMentioned(req.Text)
	userList, err := ctr.repository.ReceiveUpdate(req.Sender, mentionedList)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Mapping
	var res ReceiveUpdateRes
	res.Mapping(userList)

	return ctr.SuccessRes(c, res)
}

func (ctr *Controller) getListEmailMentioned(text string) []string {
	rgx := regexp.MustCompile(`([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)`)
	result := rgx.FindAllString(text, -1)
	if len(result) <= 0 || result == nil {
		return []string{}
	}

	return result
}
