package receiveupdate

import (
	"fmt"
	logrus "github.com/Sirupsen/logrus"
	// "github.com/jinzhu/gorm"
	api "projects/challenge/server/api"
	msg "projects/challenge/server/internal/message"
	model "projects/challenge/server/model"
)

// Repository use to interact with database
type Repository struct {
	api.RepositoryBase
}

// Subscribe do subscribe for an user
// ! No block update from sender
// ! has been @mentioned in text
// ! has subscribled sender
// ! has friend with sender
func (r *Repository) ReceiveUpdate(sender string, mentioned []string) (userList []model.User, err error) {
	r.OpenConnection()
	defer r.DB.Close()
	userList = []model.User{}

	// get sender info
	senderInfo, err := r.QueryUserInfoFromEmail(sender)
	if err != nil {
		logrus.Error("ReceiveUpdate Error: ", err)
		err = fmt.Errorf(msg.CanNotGetUserInfo)
		return
	}

	// get blocklist
	var blocklist []model.Blocklist
	err = r.DB.Table(
		"blocklist",
	).Select(
		"distinct(user_id)",
	).Where(
		"friend_id = ?", senderInfo.ID,
	).Find(&blocklist).Error

	if err != nil {
		logrus.Error("ReceiveUpdate Error: ", err)
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	blocklistIDs := r.getUserIDFromBlocklist(blocklist)
	logrus.Error(blocklistIDs)

	// get subscrible list
	var subscribleList []model.Subscription
	err = r.DB.Table(
		"subscriptions",
	).Select(
		"distinct user_id",
	).Where(
		"friend_id = ?", senderInfo.ID,
	).Not(
		"user_id", append(blocklistIDs, senderInfo.ID),
	).Find(&subscribleList).Error

	if err != nil {
		logrus.Error("ReceiveUpdate Error: ", err)
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	// get user match with condition
	err = r.DB.Table(
		"users",
	).Where(
		"id IN (?) AND id NOT IN (?)",
		r.getUserIDFromSubscriptions(subscribleList),
		append(blocklistIDs, senderInfo.ID),
	).Or(
		"email IN (?) AND id NOT IN (?)",
		mentioned,
		append(blocklistIDs, senderInfo.ID),
	).Find(&userList).Error

	if err != nil {
		userList = []model.User{}
		logrus.Error("ReceiveUpdate Error: ", err)
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	return
}

func (r *Repository) getUserIDFromBlocklist(blocklist []model.Blocklist) (userIDs []uint) {
	if len(blocklist) > 0 {
		for _, item := range blocklist {
			userIDs = append(userIDs, item.UserID)
		}
	} else {
		userIDs = []uint{}
	}

	return
}

func (r *Repository) getUserIDFromSubscriptions(subscribleList []model.Subscription) (userIDs []uint) {
	if len(subscribleList) > 0 {
		for _, item := range subscribleList {
			userIDs = append(userIDs, item.UserID)
		}
	} else {
		userIDs = []uint{}
	}

	return
}
