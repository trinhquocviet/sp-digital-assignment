package api

import (
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	model "projects/challenge/server/model"
	"reflect"
	"testing"
)

func TestRepositoryBase_QueryUserInfoFromEmail(t *testing.T) {
	tests := []struct {
		name         string
		email        string
		wantUserInfo model.User
		wantError    bool
	}{
		{
			name:         "Query get Andy info",
			email:        "andy@example.com",
			wantUserInfo: model.User{ID: 1, Email: "andy@example.com"},
			wantError:    false,
		},
		{
			name:         "Query non exist user",
			email:        "nonexistuser@example.com",
			wantUserInfo: model.User{},
			wantError:    true,
		},
		{
			name:         "Query is not email",
			email:        "andy",
			wantUserInfo: model.User{},
			wantError:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := RepositoryBase{}
			r.OpenConnection()
			defer r.DB.Close()
			rawUserInfo, err := r.QueryUserInfoFromEmail(tt.email)

			if !tt.wantError {
				if err != nil {
					t.Errorf("RepositoryBase.QueryUserInfoFromEmail() gotErr = %v, want %v", err.Error(), "nil")
				} else {
					if reflect.DeepEqual(rawUserInfo, model.User{}) {
						t.Errorf("RepositoryBase.QueryUserInfoFromEmail() incompatible")
					} else {
						userInfo := model.User{ID: rawUserInfo.ID, Email: rawUserInfo.Email}
						if !reflect.DeepEqual(userInfo, tt.wantUserInfo) {
							t.Errorf("RepositoryBase.QueryUserInfoFromEmail() incompatible")
						}
					}
				}
			} else {
				if err == nil {
					t.Errorf("RepositoryBase.QueryUserInfoFromEmail() gotErr = %v, want %v", err, tt.wantError)
				}
			}
		})
	}
}

func TestRepositoryBase_QueryUserInfoFromEmailList(t *testing.T) {
	tests := []struct {
		name          string
		emailList     []string
		wantUserInfos []model.User
		wantError     bool
	}{
		{
			name:          "Query get Andy and John",
			emailList:     []string{"andy@example.com", "john@example.com"},
			wantUserInfos: []model.User{model.User{ID: 1, Email: "andy@example.com"}, model.User{ID: 2, Email: "john@example.com"}},
			wantError:     false,
		},
		{
			name:          "Query get Andy and non exist user",
			emailList:     []string{"andy@example.com", "nonexistuser@example.com"},
			wantUserInfos: []model.User{model.User{ID: 1, Email: "andy@example.com"}},
			wantError:     false,
		},
		{
			name:          "Emaillist is empty",
			emailList:     []string{},
			wantUserInfos: []model.User{},
			wantError:     true,
		},
		{
			name:          "Query get Andy only",
			emailList:     []string{"andy@example.com"},
			wantUserInfos: []model.User{model.User{ID: 1, Email: "andy@example.com"}},
			wantError:     false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := RepositoryBase{}
			r.OpenConnection()
			defer r.DB.Close()
			rawUserInfos, err := r.QueryUserInfoFromEmailList(tt.emailList)
			userInfos := []model.User{}

			for _, u := range rawUserInfos {
				userInfos = append(userInfos, model.User{ID: u.ID, Email: u.Email})
			}

			if !tt.wantError {
				if err != nil {
					t.Errorf("RepositoryBase.QueryUserInfoFromEmailList() gotErr = %v, want %v", err.Error(), "nil")
				} else if len(userInfos) != len(tt.wantUserInfos) || !reflect.DeepEqual(userInfos, tt.wantUserInfos) {
					t.Errorf("RepositoryBase.QueryUserInfoFromEmailList() incompatible")
				}
			} else {
				if err == nil {
					t.Errorf("RepositoryBase.QueryUserInfoFromEmailList() gotErr = %v, want %v", err, tt.wantError)
				}
			}
		})
	}
}

func TestRepositoryBase_QueryFriendShipsFromUserIDList(t *testing.T) {
	tests := []struct {
		name            string
		userIDs         []uint
		wantFriendships []model.Friendship
		wantError       bool
	}{
		{
			name:            "Query get friendship of Andy and Common",
			userIDs:         []uint{1, 3},
			wantFriendships: []model.Friendship{model.Friendship{UserID: 3, FriendID: 1}},
			wantError:       false,
		},
		{
			name:            "Query get friendship of John and Vel",
			userIDs:         []uint{2, 8},
			wantFriendships: []model.Friendship{},
			wantError:       false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := RepositoryBase{}
			r.OpenConnection()
			defer r.DB.Close()
			rawFriendships, err := r.QueryFriendShipsFromUserIDList(tt.userIDs)
			friendships := []model.Friendship{}

			for _, f := range rawFriendships {
				friendships = append(friendships, model.Friendship{UserID: f.UserID, FriendID: f.FriendID})
			}

			if !tt.wantError {
				if err != nil {
					t.Errorf("RepositoryBase.QueryUserInfoFromEmailList() gotErr = %v, want %v", err.Error(), "nil")
				} else if len(friendships) != len(tt.wantFriendships) || !reflect.DeepEqual(friendships, tt.wantFriendships) {
					t.Errorf("RepositoryBase.QueryUserInfoFromEmailList() incompatible")
				}
			} else {
				if err == nil {
					t.Errorf("RepositoryBase.QueryUserInfoFromEmailList() gotErr = %v, want %v", err, tt.wantError)
				}
			}
		})
	}
}
