package friendships

import (
	model "projects/challenge/server/model"
	"reflect"
	"testing"
)

func TestRepository_GetAllFriends(t *testing.T) {
	tests := []struct {
		name         string
		email        string
		wantUserList []model.User
		wantErr      bool
	}{
		{
			name:         "Get friend of Common",
			email:        "common@example.com",
			wantUserList: []model.User{model.User{Email: "andy@example.com"}, model.User{Email: "john@example.com"}},
			wantErr:      false,
		},
		{
			name:         "Get friend of Vel",
			email:        "vel@example.com",
			wantUserList: []model.User{},
			wantErr:      false,
		},
		{
			name:         "Get friend of non exist user",
			email:        "nonexistuser@example.com",
			wantUserList: []model.User{},
			wantErr:      true,
		},
		{
			name:         "Email is blank",
			email:        "",
			wantUserList: []model.User{},
			wantErr:      true,
		},
		{
			name:         "Email is not email address",
			email:        "isnotemail",
			wantUserList: []model.User{},
			wantErr:      true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var r Repository
			rawUserList, err := r.GetAllFriends(tt.email)
			gotUserList := []model.User{}
			if len(rawUserList) > 0 {
				for _, u := range rawUserList {
					gotUserList = append(gotUserList, model.User{Email: u.Email})
				}
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("Repository.GetAllFriends() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotUserList, tt.wantUserList) {
				t.Errorf("Repository.GetAllFriends() = %v, want %v", gotUserList, tt.wantUserList)
			}
		})
	}
}

func TestRepository_CreateFriendship(t *testing.T) {
	tests := []struct {
		name    string
		friends []string
		wantErr bool
	}{
		{
			name:    "Make friend for Cup and Cafe",
			friends: []string{"cup@example.com", "cafe@example.com"},
			wantErr: false,
		},
		{
			name:    "Make friend for Cup and himself",
			friends: []string{"cup@example.com", "cup@example.com"},
			wantErr: true,
		},
		{
			name:    "Empty friends array",
			friends: []string{},
			wantErr: true,
		},
		{
			name:    "Friends only have 1 email",
			friends: []string{"cup@example.com"},
			wantErr: true,
		},
		{
			name:    "Friends have 2 text is not email address",
			friends: []string{"cup", "cafe"},
			wantErr: true,
		},
		{
			name:    "Make friend for Mentos and Cup",
			friends: []string{"mentos@example.com", "cup@example.com"},
			wantErr: true,
		},
		{
			name:    "Make friend for Mentos and non exist user",
			friends: []string{"mentos@example.com", "nonexistuser@example.com"},
			wantErr: true,
		},
		{
			name:    "Make friend for non exist user and Mentos",
			friends: []string{"nonexistuser@example.com", "mentos@example.com"},
			wantErr: true,
		},
		{
			name:    "Make friend for non exist user and non exist user",
			friends: []string{"nonexistuser1@example.com", "nonexistuser2@example.com"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var r Repository
			if err := r.CreateFriendship(tt.friends); (err != nil) != tt.wantErr {
				t.Errorf("Repository.CreateFriendship() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
