package friendships

import (
	"fmt"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	msg "projects/challenge/server/internal/message"
	"strings"
	"testing"
)

var (
	defaultSuccesResJSONForCreateFriendship = `{"success":true}`
)

func TestController_CreateFriendship(t *testing.T) {
	requestFormat := `{"friends":["%s","%s"]}`
	erroResFormat := `{"success":false,"error":"%s"}`

	tests := []struct {
		name           string
		customReqJSON  string
		wantHTTPStatus int
		wantResJSON    string
	}{
		{
			name:           "Make friend for Lisa and Kate",
			customReqJSON:  fmt.Sprintf(requestFormat, "lisa@example.com", "kate@example.com"),
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    defaultSuccesResJSONForCreateFriendship,
		},
		{
			name:           "Make friend for Andy and common",
			customReqJSON:  fmt.Sprintf(requestFormat, "andy@example.com", "common@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.InTheConnection),
		},
		{
			name:           "Make friend for Andy and himself",
			customReqJSON:  fmt.Sprintf(requestFormat, "andy@example.com", "andy@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.OneOfUsersDoNotExist),
		},
		{
			name:           "Make friend for Andy and non exist user",
			customReqJSON:  fmt.Sprintf(requestFormat, "andy@example.com", "nonexistuser@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.OneOfUsersDoNotExist),
		},
		{
			name:           "Make friend for Andy and Sit, but Sit is blocking Andy",
			customReqJSON:  fmt.Sprintf(requestFormat, "andy@example.com", "sit@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.DefaultError),
		},
		{
			name:           "Make friend for Andy and blank user",
			customReqJSON:  fmt.Sprintf(requestFormat, "andy@example.com", ""),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.RequestParamsMismatch),
		},
		{
			name:           "Make friend for blank and Andy",
			customReqJSON:  fmt.Sprintf(requestFormat, "", "andy@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.RequestParamsMismatch),
		},
		{
			name:           "Make friend with blank array",
			customReqJSON:  `{"friends":[]}`,
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.RequestParamsMismatch),
		},
		{
			name:           "Make friend with empty request",
			customReqJSON:  `{}`,
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(erroResFormat, msg.RequestParamsMismatch),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var ctr Controller
			e := echo.New()
			req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(tt.customReqJSON))
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			rec := httptest.NewRecorder()
			c := e.NewContext(req, rec)

			if assert.NoError(t, ctr.CreateFriendship(c)) {
				assert.Equal(t, tt.wantHTTPStatus, rec.Code)
				assert.Equal(t, tt.wantResJSON, rec.Body.String())
			}
		})
	}
}

func TestController_GetAllFriends(t *testing.T) {
	requestFormat := `{"email":"%s"}`
	errorResFormat := `{"success":false,"error":"%s"}`

	tests := []struct {
		name           string
		customReqJSON  string
		wantHTTPStatus int
		wantResJSON    string
	}{
		{
			name:           "Get friend of Common",
			customReqJSON:  fmt.Sprintf(requestFormat, "common@example.com"),
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    `{"success":true,"friends":["andy@example.com","john@example.com"],"count":2}`,
		},
		{
			name:           "Get friend of non exist user",
			customReqJSON:  fmt.Sprintf(requestFormat, "nonexistuser@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.UserDoesNotExist),
		},
		{
			name:           "Get friend of Vel but Vel has no friend",
			customReqJSON:  fmt.Sprintf(requestFormat, "vel@example.com"),
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    `{"success":true,"friends":[],"count":0}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var ctr Controller
			e := echo.New()
			req := httptest.NewRequest(http.MethodGet, "/", strings.NewReader(tt.customReqJSON))
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			rec := httptest.NewRecorder()
			c := e.NewContext(req, rec)

			if assert.NoError(t, ctr.GetAllFriends(c)) {
				assert.Equal(t, tt.wantHTTPStatus, rec.Code)
				assert.Equal(t, tt.wantResJSON, rec.Body.String())
			}
		})
	}
}
