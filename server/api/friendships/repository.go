package friendships

import (
	"fmt"
	logrus "github.com/Sirupsen/logrus"
	// "github.com/jinzhu/gorm"
	api "projects/challenge/server/api"
	msg "projects/challenge/server/internal/message"
	model "projects/challenge/server/model"
)

// Repository use to interact with database
type Repository struct {
	api.RepositoryBase
}

// CreateFriendship create the friendship bet
// ! If userB is blocked by userA, userB can't make friend with user A
// ! If userA blocked userB, when userA make friend with userB will unblock
// ! If make the friendship, insert subscription for userA and userB
func (r *Repository) CreateFriendship(friends []string) (err error) {
	r.OpenConnection()
	defer r.DB.Close()

	// Search userlist
	userInfos, err := r.QueryUserInfoFromEmailList(friends)

	if err != nil || len(userInfos) != 2 {
		logrus.Error("CreateFriendship error: ", err)
		err = fmt.Errorf(msg.OneOfUsersDoNotExist)
		return
	}

	// Search if has friendship already
	friendships, err := r.QueryFriendShipsFromUserIDList([]uint{userInfos[0].ID, userInfos[1].ID})

	if err != nil {
		logrus.Error("CreateFriendship error: ", err)
		err = fmt.Errorf(msg.DefaultError)
		return
	} else if len(friendships) > 0 {
		logrus.Error("CreateFriendship error: too many friendships")
		err = fmt.Errorf(msg.InTheConnection)
		return
	}

	// Search if requestor blocked by target
	var count int
	err = r.DB.Table(
		"blocklist",
	).Where(
		"friend_id = ? AND user_id = ?", userInfos[0].ID, userInfos[1].ID,
	).Count(&count).Error

	if err != nil || count > 0 {
		logrus.Error("CreateFriendship Error: blocking rule - ", err)
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	// Do inseart to friendships and subscriptions
	tx := r.DB.Begin()
	// make the friendship
	err = tx.Table(
		"friendships",
	).Create(
		&model.Friendship{UserID: userInfos[0].ID, FriendID: userInfos[1].ID},
	).Error

	if err != nil {
		tx.Rollback()
		logrus.Error("CreateFriendship Error: ", err)
		err = fmt.Errorf(msg.CanNotCreateConnection)
		return
	}

	// make the subscription
	err = tx.Exec(
		`INSERT OR IGNORE INTO subscriptions (user_id, friend_id) VALUES (?,?);
		INSERT OR IGNORE INTO subscriptions (user_id, friend_id) VALUES (?,?);`,
		userInfos[0].ID, userInfos[1].ID, userInfos[1].ID, userInfos[0].ID,
	).Error

	if err != nil {
		tx.Rollback()
		logrus.Error("CreateFriendship Error: ", err)
		err = fmt.Errorf(msg.CanNotCreateConnection)
		return
	}

	// remove block if the requester blocked the target
	err = tx.Delete(
		model.Blocklist{},
		"user_id = ? AND friend_id = ?", userInfos[0].ID, userInfos[1].ID,
	).Error

	if err != nil {
		tx.Rollback()
		logrus.Error("CreateFriendship Error: ", err)
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	tx.Commit()
	return
}

// GetAllFriends query to get all friend of email
func (r *Repository) GetAllFriends(email string) (userList []model.User, err error) {
	r.OpenConnection()
	defer r.DB.Close()
	userList = []model.User{}

	// get user info
	userInfo, err := r.QueryUserInfoFromEmail(email)
	if err != nil {
		logrus.Error("GetAllFriends Error: ", err)
		return
	}

	// get list friends
	queryExpr := r.DB.Table("friendships").Select(
		"CASE WHEN user_id = ? THEN friend_id ELSE user_id END", userInfo.ID,
	).Where(
		"user_id = ?", userInfo.ID,
	).Or(
		"friend_id = ?", userInfo.ID,
	).QueryExpr()

	err = r.DB.Table(
		"users",
	).Select(
		"email",
	).Where(
		"id IN (?)", queryExpr,
	).Find(&userList).Error

	if err != nil {
		logrus.Error("GetAllFriends Error: ", err)
		userList = []model.User{}
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	return
}
