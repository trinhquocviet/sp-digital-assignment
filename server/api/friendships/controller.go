package friendships

import (
	"github.com/labstack/echo"
	api "projects/challenge/server/api"
	internal "projects/challenge/server/internal"
)

// Controller the handler function extend from api/controller-base.go
type Controller struct {
	api.ControllerBase
	repository Repository
	validator  internal.Validator
}

// CreateFriendship will create a new friendship between 2 users
func (ctr *Controller) CreateFriendship(c echo.Context) error {
	// Build query
	var req CreateFriendshipReq
	if err := c.Bind(&req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Validate
	if err := ctr.validator.Validate(req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Execute
	if err := ctr.repository.CreateFriendship(req.Friends); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	return ctr.SuccessRes(c)
}

// GetAllFriends will fetch and get all user from database
func (ctr *Controller) GetAllFriends(c echo.Context) error {
	// Build query
	var req GetAllFriendsReq
	if err := c.Bind(&req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Validate
	if err := ctr.validator.Validate(req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Execute
	userList, err := ctr.repository.GetAllFriends(req.Email)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Mapping
	var res GetAllFriendsRes
	res.Mapping(userList)

	return ctr.SuccessRes(c, res)
}
