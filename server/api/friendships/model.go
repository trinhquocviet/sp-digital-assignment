package friendships

import (
	api "projects/challenge/server/api"
	model "projects/challenge/server/model"
)

// CreateFriendshipReq the Struct for binding data from request
type CreateFriendshipReq struct {
	Friends []string `json:"friends" validate:"required,len=2,dive,email"`
}

// GetAllFriendsReq the Struct for binding data from request
type GetAllFriendsReq struct {
	Email string `json:"email" validate:"required,email"`
}

// GetAllFriendsRes the struct is response
type GetAllFriendsRes struct {
	api.DefaultSuccessRes
	Friends []string `json:"friends" validate:"required,dive,email"`
	Count   int      `json:"count" validate:"required,numeric"`
}

// Mapping is the function to map from model to response model
func (g *GetAllFriendsRes) Mapping(userList []model.User) {
	g.Success = true
	if len(userList) > 0 {
		for _, user := range userList {
			g.Friends = append(g.Friends, user.Email)
		}
	} else {
		g.Friends = []string{}
	}
	g.Count = len(userList)
}
