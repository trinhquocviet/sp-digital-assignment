package subscribe

import (
	"github.com/labstack/echo"
	api "projects/challenge/server/api"
	internal "projects/challenge/server/internal"
)

// Controller the handler function extend from api/controller_base.go
type Controller struct {
	api.ControllerBase
	repository Repository
	validator  internal.Validator
}

// Subscribe will make subscription for other user
func (ctr *Controller) Subscribe(c echo.Context) error {
	// Build query
	var req SubscribeReq
	if err := c.Bind(&req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Validate
	if err := ctr.validator.Validate(req); err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	// Execute
	err := ctr.repository.Subscribe(req)
	if err != nil {
		return ctr.ErrorRes(c, err.Error())
	}

	return ctr.SuccessRes(c)
}
