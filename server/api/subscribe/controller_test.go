package subscribe

import (
	"fmt"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	msg "projects/challenge/server/internal/message"
	"strings"
	"testing"
)

var (
	defaultSuccesResJSONForSubscribeUser = `{"success":true}`
)

func TestController_Subscribe(t *testing.T) {
	requestFormat := `{"requestor": "%s","target":"%s"}`
	errorResFormat := `{"success":false,"error":"%s"}`

	tests := []struct {
		name           string
		reqJSON        string
		wantHTTPStatus int
		wantResJSON    string
	}{
		{
			name:           "Andy subscribe Common, Andy is a friend with Common",
			reqJSON:        fmt.Sprintf(requestFormat, "andy@example.com", "john@example.com"),
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    defaultSuccesResJSONForSubscribeUser,
		},
		{
			name:           "Andy subscribe non exist user",
			reqJSON:        fmt.Sprintf(requestFormat, "andy@example.com", "nonexistuser@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.OneOfUsersDoNotExist),
		},
		{
			name:           "Andy subscribe himself",
			reqJSON:        fmt.Sprintf(requestFormat, "andy@example.com", "andy@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.OneOfUsersDoNotExist),
		},
		{
			name:           "Non exist subscribe block Andy",
			reqJSON:        fmt.Sprintf(requestFormat, "nonexistuser@example.com", "andy@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.OneOfUsersDoNotExist),
		},
		{
			name:           "Andy subscribe Vel, Andy isn't friend with Vel",
			reqJSON:        fmt.Sprintf(requestFormat, "andy@example.com", "vel@example.com"),
			wantHTTPStatus: http.StatusOK,
			wantResJSON:    defaultSuccesResJSONForSubscribeUser,
		},
		{
			name:           "blank request",
			reqJSON:        `{}`,
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.RequestParamsMismatch),
		},
		{
			name:           "blank requestor subscribe Vel",
			reqJSON:        fmt.Sprintf(requestFormat, "", "vel@example.com"),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.RequestParamsMismatch),
		},
		{
			name:           "Andy subscribe blank target",
			reqJSON:        fmt.Sprintf(requestFormat, "andy@example.com", ""),
			wantHTTPStatus: http.StatusBadRequest,
			wantResJSON:    fmt.Sprintf(errorResFormat, msg.RequestParamsMismatch),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var ctr Controller
			e := echo.New()
			req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(tt.reqJSON))
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			rec := httptest.NewRecorder()
			c := e.NewContext(req, rec)

			if assert.NoError(t, ctr.Subscribe(c)) {
				assert.Equal(t, tt.wantHTTPStatus, rec.Code)
				assert.Equal(t, tt.wantResJSON, rec.Body.String())
			}
		})
	}
}
