package subscribe

import (
	"fmt"
	logrus "github.com/Sirupsen/logrus"
	api "projects/challenge/server/api"
	msg "projects/challenge/server/internal/message"
	model "projects/challenge/server/model"
)

// Repository use to interact with database
type Repository struct {
	api.RepositoryBase
}

// Subscribe do subscribe for an user
// ! If userB is blocked by userA, userB can't subscribe user A
// ! If userA blocked userB, when userA re-subscribe userB will unblock
func (r *Repository) Subscribe(req SubscribeReq) (err error) {
	r.OpenConnection()
	defer r.DB.Close()

	// get user info
	userInfos, err := r.QueryUserInfoFromEmailList([]string{req.Requestor, req.Target})

	if err != nil || len(userInfos) != 2 {
		logrus.Error("Subscribe Error: ", err)
		err = fmt.Errorf(msg.OneOfUsersDoNotExist)
		return
	}

	// Search if the requester is blocked by the target
	var count int
	err = r.DB.Table(
		"blocklist",
	).Where(
		"(friend_id = ? AND user_id = ?)", userInfos[0].ID, userInfos[1].ID,
	).Count(&count).Error

	if err != nil || count > 0 {
		logrus.Error("Subscribe Error: blocking rule - ", err)
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	// make subscriptions
	tx := r.DB.Begin()

	err = tx.Exec(
		`INSERT OR IGNORE INTO subscriptions (user_id, friend_id) VALUES (?,?)`,
		userInfos[0].ID, userInfos[1].ID,
	).Error

	if err != nil {
		tx.Rollback()
		logrus.Error("Subscribe Error: ", err)
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	// remove block if the requester blocked the target
	err = tx.Table(
		"blocklist",
	).Where(
		"(user_id = ? AND friend_id = ?)", userInfos[0].ID, userInfos[1].ID,
	).Delete(&model.Blocklist{}).Error

	if err != nil {
		tx.Rollback()
		logrus.Error("Subscribe Error: ", err)
		err = fmt.Errorf(msg.DefaultError)
		return
	}

	tx.Commit()
	return
}
