package subscribe

// SubscribeReq the Struct for binding data from request
type SubscribeReq struct {
	Requestor string `json:"requestor" validate:"required,email"`
	Target    string `json:"target" validate:"required,email"`
}
