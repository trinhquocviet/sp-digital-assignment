package subscribe

import (
	"testing"
)

func TestRepository_Subscribe(t *testing.T) {
	tests := []struct {
		name    string
		req     SubscribeReq
		wantErr bool
	}{
		{
			name:    "Phone subscribe sock",
			req:     SubscribeReq{Requestor: "phone@example.com", Target: "sock@example.com"},
			wantErr: false,
		},
		{
			name:    "Phone subscribe himself",
			req:     SubscribeReq{Requestor: "phone@example.com", Target: "phone@example.com"},
			wantErr: true,
		},
		{
			name:    "Empty request",
			req:     SubscribeReq{},
			wantErr: true,
		},
		{
			name:    "Magna subscribe Sit",
			req:     SubscribeReq{Requestor: "magna@example.com", Target: "sit@example.com"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var r Repository
			if err := r.Subscribe(tt.req); (err != nil) != tt.wantErr {
				t.Errorf("Repository.Subscribe() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
