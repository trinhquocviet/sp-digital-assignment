package api

import (
	"fmt"
	logrus "github.com/Sirupsen/logrus"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"os"
	msg "projects/challenge/server/internal/message"
	model "projects/challenge/server/model"
	// "reflect"
)

// RepositoryBase the base class for api API class
type RepositoryBase struct {
	DB *gorm.DB
}

// OpenConnection the function to init the db connection and assign to Base.DB
func (r *RepositoryBase) OpenConnection() (err error) {
	r.DB, err = gorm.Open("sqlite3", os.Getenv("API_DATABASE"))
	return
}

// QueryUserInfoFromEmail the function return userInfo based on email
func (r *RepositoryBase) QueryUserInfoFromEmail(email string) (userInfo model.User, err error) {
	err = r.DB.Table("users").Where("email = ?", email).First(&userInfo).Error

	if err != nil {
		logrus.Error("QueryUserInfoFromEmail error: ", err)
		err = fmt.Errorf(msg.UserDoesNotExist)
		return
	}
	return
}

// QueryUserInfoFromEmailList the function return userInfos based on email list
func (r *RepositoryBase) QueryUserInfoFromEmailList(emailList []string) (userInfos []model.User, err error) {
	if len(emailList) > 0 {
		err = r.DB.Table(
			"users",
		).Where(
			"email IN (?)", emailList,
		).Order(
			gorm.Expr("email = ? DESC", emailList[0]),
		).Find(&userInfos).Error

		if err != nil {
			logrus.Error("QueryUserInfoFromEmailList error: ", err)
			err = fmt.Errorf(msg.UserDoesNotExist)
			userInfos = []model.User{}
			return
		}

		if len(userInfos) <= 0 {
			err = nil
			userInfos = []model.User{}
			return
		}
	} else {
		err = fmt.Errorf(msg.UserDoesNotExist)
	}
	return
}

// QueryFriendShipsFromUserIDList the function return Friendship list based on user id list
func (r *RepositoryBase) QueryFriendShipsFromUserIDList(UserIDs []uint) (friendships []model.Friendship, err error) {
	if len(UserIDs) == 2 {
		err = r.DB.Table(
			"friendships",
		).Where(
			"(user_id = ? AND friend_id = ?)", UserIDs[0], UserIDs[1],
		).Or(
			"(friend_id = ? AND user_id = ?)", UserIDs[0], UserIDs[1],
		).Find(&friendships).Error

		if err != nil {
			logrus.Error("QueryFriendShipsFromUserIDList error: ", err)
			friendships = []model.Friendship{}
			return
		}

		if len(friendships) <= 0 {
			err = nil
			friendships = []model.Friendship{}
			return
		}
	} else {
		friendships = []model.Friendship{}
		err = fmt.Errorf(msg.DefaultError)
		logrus.Error("QueryFriendShipsFromUserIDList error: query UserIDs != 2")
	}

	return
}
