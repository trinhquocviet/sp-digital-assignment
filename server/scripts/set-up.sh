#!/bin/bash
# glide install
go get -u
echo  "${API_DATABASE}"
goose -dir migrations sqlite3 "${API_DATABASE}" up
go run main.go