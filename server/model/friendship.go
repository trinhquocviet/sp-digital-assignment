package model

// Friendship the data transfer object from database
type Friendship struct {
	ID       uint `gorm:"column:id"`
	UserID   uint `gorm:"column:user_id"`
	FriendID uint `gorm:"column:friend_id"`
}
