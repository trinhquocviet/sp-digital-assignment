package model

// Blocklist the data transfer object from database
type Blocklist struct {
	ID       uint `gorm:"column:id"`
	UserID   uint `gorm:"column:user_id"`
	FriendID uint `gorm:"column:friend_id"`
}

// TableName define name for gorm Table
func (b *Blocklist) TableName() string {
	return "blocklist"
}
