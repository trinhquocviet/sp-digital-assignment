package model

// Subscription the data transfer object from database
type Subscription struct {
	ID       uint `gorm:"column:id"`
	UserID   uint `gorm:"column:user_id"`
	FriendID uint `gorm:"column:friend_id"`
}
