module projects/challenge/server

go 1.13

require (
	github.com/Sirupsen/logrus v1.0.5
	github.com/dgrijalva/jwt-go v3.2.1-0.20190620180102-5e25c22bd5d6+incompatible
	github.com/go-playground/locales v0.12.2-0.20190430153329-630ebbb60284
	github.com/go-playground/universal-translator v0.16.1-0.20170327191703-71201497bace
	github.com/jinzhu/gorm v1.9.12-0.20191008100452-1bca5dbdd9e8
	github.com/jinzhu/inflection v1.0.0
	github.com/labstack/echo v3.3.5+incompatible
	github.com/labstack/gommon v0.3.0
	github.com/leodido/go-urn v1.2.0
	github.com/mattn/go-colorable v0.1.4
	github.com/mattn/go-isatty v0.0.11-0.20191009155615-0e9ddb7c0c0a
	github.com/mattn/go-sqlite3 v1.11.1-0.20191008083825-3f45aefa8dc8
	github.com/sandalwing/echo-logrusmiddleware v0.0.0-20161226034227-1d7700fcf2d3
	github.com/stretchr/testify v1.4.0
	github.com/valyala/bytebufferpool v1.0.1-0.20180905182247-cdfbe9377474
	github.com/valyala/fasttemplate v1.1.0
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	golang.org/x/net v0.0.0-20191014212845-da9a3fd4c582
	golang.org/x/sys v0.0.0-20191010194322-b09406accb47
	golang.org/x/text v0.3.3-0.20190829152558-3d0f7978add9
	gopkg.in/airbrake/gobrake.v2 v2.0.9 // indirect
	gopkg.in/gemnasium/logrus-airbrake-hook.v2 v2.1.2 // indirect
	gopkg.in/go-playground/validator.v9 v9.30.0
)
