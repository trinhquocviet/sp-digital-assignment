package message

// DefaultError response error
const DefaultError = "an error has occurred. please try again later"

// OneOfUsersDoNotExist an error when can't find user or only can find 1 user
const OneOfUsersDoNotExist = "one of the users doesn't exist or is duplicate"

// InTheConnection an error when two user inside the connection
const InTheConnection = "two users are in the connection"

// CanNotCreateConnection an error when can't create connection
const CanNotCreateConnection = "can't create connection, please try again later"

// UserDoesNotExist an error when get empty user
const UserDoesNotExist = "user doesn't exist"

// CanNotBlockAUser an error when block user
const CanNotBlockAUser = "an error has occurred cause can't block the user"

// CanNotGetUserInfo an error when user is not exist
const CanNotGetUserInfo = "can't get user info, user isn't exist"

// RequestParamsMismatch an error when request is mismatch
const RequestParamsMismatch = "request params are mismatch, please try again"
