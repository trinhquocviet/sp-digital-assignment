package routes

import (
	"github.com/labstack/echo"
	blockFriend "projects/challenge/server/api/block"
	commonFriends "projects/challenge/server/api/commonfriends"
	friendships "projects/challenge/server/api/friendships"
	receiveupdateFromFriend "projects/challenge/server/api/receiveupdate"
	subscribeFriend "projects/challenge/server/api/subscribe"
)

// SetupAPIRoutes is main function to start to setup routes
func SetupAPIRoutes(e *echo.Echo) {
	root := e.Group("/friend-management")

	friendManagementRoutes(root)
	commonFriendsRoutes(root)
	subscribeRoutes(root)
	blockRoutes(root)
	receiveUpdateRoutes(root)
}

func friendManagementRoutes(group *echo.Group) {
	var ctr friendships.Controller

	group.POST("", ctr.CreateFriendship)
	group.GET("", ctr.GetAllFriends)
}

func commonFriendsRoutes(group *echo.Group) {
	var ctr commonFriends.Controller

	group.GET("/common-friends", ctr.GetCommonFriends)
}

func subscribeRoutes(group *echo.Group) {
	var ctr subscribeFriend.Controller

	group.POST("/subscribe", ctr.Subscribe)
}

func blockRoutes(group *echo.Group) {
	var ctr blockFriend.Controller

	group.POST("/block", ctr.Block)
}

func receiveUpdateRoutes(group *echo.Group) {
	var ctr receiveupdateFromFriend.Controller

	group.GET("/receive-update", ctr.ReceiveUpdate)
}
