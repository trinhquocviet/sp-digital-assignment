-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS "users" (
  "id"          INTEGER NOT NULL PRIMARY KEY,
  "email"       VARCHAR(255) NOT NULL UNIQUE,
  "created_at"  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at"  DATETIME,
  "deleted_at"  DATETIME
);

CREATE TABLE IF NOT EXISTS "friendships" (
  "id"          INTEGER NOT NULL PRIMARY KEY,
  "user_id"     INTEGER NOT NULL,
  "friend_id"   INTEGER NOT NULL,

  UNIQUE(user_id, friend_id),
  FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE NO ACTION,
  FOREIGN KEY (friend_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS "subscriptions" (
  "id"          INTEGER NOT NULL PRIMARY KEY,
  "user_id"     INTEGER NOT NULL,
  "friend_id"   INTEGER NOT NULL,

  UNIQUE(user_id, friend_id),
  FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE NO ACTION,
  FOREIGN KEY (friend_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS "blocklist" (
  "id"          INTEGER NOT NULL PRIMARY KEY,
  "user_id"     INTEGER NOT NULL,
  "friend_id"   INTEGER NOT NULL,

  UNIQUE(user_id, friend_id),
  FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE NO ACTION,
  FOREIGN KEY (friend_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE NO ACTION
);

INSERT INTO users (email) VALUES ("andy@example.com"),("john@example.com"),("common@example.com"),("lisa@example.com"),("kate@example.com");
INSERT INTO users (email) VALUES ("magna@example.com"),("sit@example.com"),("pede@example.com"),("vel@example.com"),("corper@example.com");
INSERT INTO users (email) VALUES ("phone@example.com"),("sock@example.com"),("cafe@example.com"),("cup@example.com"),("mentos@example.com");

INSERT INTO friendships (user_id, friend_id) VALUES (3,1),(3,2),(7,2),(6,8);
INSERT INTO subscriptions (user_id, friend_id) VALUES (3,1),(1,3),(3,2),(2,3),(6,2),(6,8),(8,6);
INSERT INTO blocklist (user_id, friend_id) VALUES (7,1),(7,6),(14,15);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS "users";
DROP TABLE IF EXISTS "friendships";
DROP TABLE IF EXISTS "subscriptions";
DROP TABLE IF EXISTS "blocklist";
-- +goose StatementEnd