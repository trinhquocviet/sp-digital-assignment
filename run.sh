#!/bin/sh
## Script to run your application
## e.g. 
## npm start
## mvn clean spring-boot:run
## python -m SimpleHTTPServer 8000
## bundle exec rails server -b 0.0.0.0 -p 8000
## go run main.go
## php -S 0.0.0.0:8000

# Change directory to server
cd server

# export enviroment
export $(grep -v '^#' ../.env | xargs)

# run migration
$GOPATH/bin/goose -dir migrations sqlite3 "${API_DATABASE}" up

# Start server
go run main.go